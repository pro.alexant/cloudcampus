<?php
/**
 * labriquefilms.com functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package labriquefilms.com
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'labriquefilms_com_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function labriquefilms_com_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on labriquefilms.com, use a find and replace
		 * to change 'labriquefilms-com' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'labriquefilms-com', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'labriquefilms-com' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'labriquefilms_com_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'labriquefilms_com_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function labriquefilms_com_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'labriquefilms_com_content_width', 640 );
}
add_action( 'after_setup_theme', 'labriquefilms_com_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function labriquefilms_com_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'labriquefilms-com' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'labriquefilms-com' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'labriquefilms_com_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function labriquefilms_com_scripts() {
	wp_enqueue_style( 'labriquefilms-com-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'labriquefilms-com-style', 'rtl', 'replace' );

	wp_enqueue_script( 'labriquefilms-com-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'labriquefilms_com_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Custom Function
 */
require_once get_template_directory() . '/script.php';
require_once get_template_directory() . '/filter.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

function wpm_custom_post_type() {

	// On rentre les différentes dénominations de notre custom post type qui seront affichées dans l'administration
	$labels = array(
		// Le nom au pluriel
		'name'                => _x( 'Films', 'Post Type General Name'),
		// Le nom au singulier
		'singular_name'       => _x( 'Film', 'Post Type Singular Name'),
		// Le libellé affiché dans le menu
		'menu_name'           => __( 'Films'),
		// Les différents libellés de l'administration
		'all_items'           => __( 'Toutes les Films'),
		'view_item'           => __( 'Voir les Films'),
		'add_new_item'        => __( 'Ajouter une nouvelle Film'),
		'add_new'             => __( 'Ajouter'),
		'edit_item'           => __( 'Editer la Films'),
		'update_item'         => __( 'Modifier la Films'),
		'search_items'        => __( 'Rechercher une Film'),
		'not_found'           => __( 'Non trouvée'),
		'not_found_in_trash'  => __( 'Non trouvée dans la corbeille'),
	);

	// On peut définir ici d'autres options pour notre custom post type

	$args = array(
		'label'               => __( 'Films'),
		'description'         => __( 'Toute les Films'),
		'labels'              => $labels,
		'menu_icon'      => 'dashicons-format-video',
		'menu_position' => 3,
		// On définit les options disponibles dans l'éditeur de notre custom post type ( un titre, un auteur...)
		'supports'            => array( 'title', 'category' , 'the_content','thumbnail','revisions', 'custom-fields', 'sticky_post' ),
		/* 
		* Différentes options supplémentaires
		*/
		'taxonomies' => array('category'),
		'show_in_rest' => true,
		'hierarchical'        => false,
		'public'              => true,
		'has_archive'         => true,
		'rewrite'			  => array( 'slug' => 'movies'),
	);

	// On enregistre notre custom post type qu'on nomme ici "serietv" et ses arguments
	register_post_type( 'movies', $args );

}

function admin_css() {
	$admin_handle = 'admin_css';
	$admin_stylesheet = get_template_directory_uri() . '/admin.css';

	wp_enqueue_style($admin_handle, $admin_stylesheet);
}
add_action('admin_print_styles', 'admin_css', 11);

function remove_links_tab_menu_pages()
{
    remove_menu_page('link-manager.php');
	remove_menu_page('edit.php');
	remove_menu_page('upload.php');
	remove_menu_page('edit-comments.php');
	remove_menu_page('themes.php');
	remove_menu_page('plugins.php');
	remove_menu_page('users.php');
	remove_menu_page('tools.php');
	remove_menu_page('options-general.php');
}
add_action('admin_menu', 'remove_links_tab_menu_pages');


function my_custom_login(){
	echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/login/custom-login-style.css"/>';
}
add_action('login_head','my_custom_login');



add_action( 'init', 'wpm_custom_post_type', 0 );
add_action( 'admin_footer-post.php', 'gkp_add_sticky_post_support' );
add_action( 'admin_footer-post-new.php', 'gkp_add_sticky_post_support' );
function gkp_add_sticky_post_support() 
{ global $post, $typenow; ?>
	
	<?php if ( $typenow == 'movies' && current_user_can( 'edit_others_posts' ) ) : ?>
	<script>
	jQuery(function($) {
		var sticky = "<br/><span id='sticky-span'><input id='sticky' name='sticky' type='checkbox' value='sticky' <?php checked( is_sticky( $post->ID ) ); ?> /> <label for='sticky' class='selectit'><?php _e( "Stick this post to the front page" ); ?></label><br /></span>";	
		$('[for=visibility-radio-public]').append(sticky);	
	});
	</script>
	<?php endif; ?>
	


<?php
}

