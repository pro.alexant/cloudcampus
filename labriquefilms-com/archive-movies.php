<?php

/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package labriquefilms.com
 */

get_header();

?>

<?php
$projects = new WP_Query([
	'post_type' => 'movies',
	'posts_per_page' => -1,
	'order_by' => 'date',
	'order' => 'desc',
]);
$cat_args = array(
	'option_all' => 'All',
	'hide_empty'      => false,
);
?>

<?php $categories = get_categories($cat_args); ?>
<section id="liste-film">
	<div class="container">
		<div class="categorymobile">
			<div id="all" class="mobile"><a class="cat-list_item active" href="#!" data-slug="<?= $category->slug; ?>">Tous</a></div>
			<span class='mobile'>
				<p>FILTRE</p>
				<div class="plus icon"></div>
			</span>
		</div>
		<div id="categories">
			<ul class="cat-list">
				<div class="plus icon"></div>
				<li><a class="cat-list_item active" href="#!" data-slug="<?= $category->slug; ?>">Tous</a></li>
				<?php foreach ($categories as $category) : ?>
					<li>
						<a class="cat-list_item" href="#!" data-slug="<?= $category->slug; ?>">
							<?= $category->name; ?>
						</a>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
		<?php if ($projects->have_posts()) : ?>
			<div class="project-tiles">
				<?php
				while ($projects->have_posts()) : $projects->the_post();
					include('template-parts/film-list.php');
				endwhile;
				?>
			</div>
			<?php wp_reset_postdata(); ?>
	</div>
</section>


<?php endif; ?>
<?php get_footer() ?>