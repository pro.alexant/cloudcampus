<div id="diapo<?= $i ?>" data-section="<?= $i ?>" class="img" >
    <div class="col-15">
        <h1>Fresh News</h1>
        <div id="col-80" class="mobile">
            <img class="hovered" src=" <?php the_post_thumbnail_url() ?>"> 
            <div class=" middle">
                <div class="text"><img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-player.png" alt=""></div>
            </div><!--#col-80.mobile-->
        </div><!--.col-15-->
        <p class="uppercase"><?php the_title() ?></p>
        <div class="positionslide mobile">
            <div id="nbDiapo" class="colonne-2">
                <div><?= $i ?>/3</div>
            </div><!--.nbDiapo-->
            <div id="arrowDiapo" class="colonne-2">
                <div class="right-arrow"><img src="<?= get_stylesheet_directory_uri(); ?>/img/right-arrow.png" alt=""></div>
            </div><!--.arrowDiapo-->
        </div><!--#positionSlide-->
    </div><!--#diapo-->
    <div id="fullscreen" class="col-80">
        <img class="hovered" src=" <?php the_post_thumbnail_url() ?>">
        <div class=" middle">
            <div class="text"><img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-player.png" alt=""></div>
        </div><!--.middle-->
    </div><!--#fullscreen.col-80-->

    <div class="modal">
        <div id="frame" class="mobile vimeo">
            <?= get_field('lien_vimeo') ?>
        </div><!--#frame.mobile-->
        <div id="title-film">
            <h3><?php the_title() ?></h3>
            <h4><?php the_category(' '); ?></h4>
        </div><!--#title-film-->
        <div id="frame" class="fullscreen vimeo">
            <?= get_field('lien_vimeo') ?>
        </div><!--#frame.fullscreen-->
        <div id="retour" class="fullscreen retour">
            <a href="<?php echo site_url(); ?>/films/"><img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-plus.png" alt="Retour a la liste de Film"></a>
        </div><!--#retour.fullscreen-->
        <div class="clear"></div>
        <div id="infos-prod">
            <p class='fullscreen'><?= get_field('realisateur') ?></p>
            <div class="mobile">
                <p><?= get_field('realisateur') ?>
                </p>
            </div><!--.mobile-->
        </div><!--#infos-prod-->
        <div id="retour" class="mobile retour">
            <a href="<?php echo site_url(); ?>/films/"><img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-plus.png" alt="Retour a la liste de Film"></a>
        </div><!--#retour.mobile-->
    </div><!--#modal-->
    <div class="clear"></div>
</div><!--.img-->