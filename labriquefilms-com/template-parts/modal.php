<div class="modal">
    <div id="title-film">
        <h3><?php the_title() ?></h3>
        <h4><?php the_category( ' ' ); ?></h4>
    </div>
    <div id="frame">
        <?= get_field('lien_vimeo') ?>
    </div>
    <div id="croix">
        <a href="<?php echo site_url(); ?>/films/"><img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-plus.png" alt="Retour a la liste de Film"></a>
    </div>
    <div class="clear"></div>
</div>
<div id="infos-prod">
    <p><?= get_field('realisateur') ?> </p>
</div>