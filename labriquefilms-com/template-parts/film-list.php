<article>
        <a href="<?php the_permalink() ?>"><?php the_post_thumbnail('medium'); ?>
            <div class="centrer">
                <div class="play"><img src="<?= get_stylesheet_directory_uri(); ?>/img/icon_player.png" alt=""></div>
            </div>
        </a>
        <h3 class="uppercase"><?php the_title() ?></h3>
    </article>
