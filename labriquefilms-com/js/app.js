$(document).ready(function () {
    /************************************  BOUNCE EFFET */
    function preventPullToRefresh(element) {
        var prevent = false;

        document.querySelector(element).addEventListener('touchstart', function (e) {
            if (e.touches.length !== 1) { return; }

            var scrollY = window.pageYOffset || document.body.scrollTop || document.documentElement.scrollTop;
            prevent = (scrollY === 0);
        });

        document.querySelector(element).addEventListener('touchmove', function (e) {
            if (prevent) {
                prevent = false;
                e.preventDefault();
            }
        });
    }

    /************************************  BEN BUTTON HOVER */
    $("#ben-plus").hover(function () {
        $("#lateam").css("opacity", "0");
        $("#laben").css("opacity", "1");
        $("#lanils").css("opacity", "0");
    }, function () {
        $("#lateam").css("opacity", "1");
        $("#laben").css("opacity", "0");
        $("#lanils").css("opacity", "0");
    });
    /************************************  NILS BUTTON HOVER */
    $("#nils-plus").hover(function () {
        $("#lateam").css("opacity", "0");
        $("#laben").css("opacity", "0");
        $("#lanils").css("opacity", "1");
    }, function () {
        $("#lateam").css("opacity", "1");
        $("#laben").css("opacity", "0");
        $("#lanils").css("opacity", "0");
    });

    /*************************************** SMOOTHSCROLL */
    $('a[href^="#"]').click(function (e) {
        e.preventDefault();
        var the_id = $(this).attr("href");
        $('html, body').animate({
            scrollTop: $(the_id).offset().top
        }, 'slow');
        return false;
    }); /* END SMOOTHSCROLL */

    /*************************************** SLIDER */
    $('#diapo1').addClass("active");


    /*************************************** SLIDER */
    $('.middle').click(function () {
        $(".col-15").css("display", "none");
        $(".col-80").css("display", "none");
        $(".modal").css("display", "block");
        $("header").css("display", "none");
        $("footer").css("display", "none");
        if (window.matchMedia("(min-width: 1330px)").matches) {
            $("#scroll").css("display", "none");
        }
    });

    $('.retour').click(function (e) {
        e.preventDefault();
        $(".col-15").css("display", "block");
        $(".col-80").css("display", "block");
        $(".modal").css("display", "none");
        $("header").css("display", "block");
        $("footer").css("display", "block");
        var $frame1 = $('#diapo1 #frame iframe');
        var vidsrc1 = $frame1.attr('src');
        $frame1.attr('src', '');
        $frame1.attr('src', vidsrc1);
        var $frame2 = $('#diapo2 #frame iframe');
        var vidsrc2 = $frame2.attr('src');
        $frame2.attr('src', '');
        $frame2.attr('src', vidsrc2);
        var $frame3 = $('#diapo3 #frame iframe');
        var vidsrc3 = $frame3.attr('src');
        $frame3.attr('src', '');
        $frame3.attr('src', vidsrc3);
        if (window.matchMedia("(min-width: 1330px)").matches) {
            $("#scroll").css("display", "block");
        }
    });


    $('#title-film > h4 > a').click(function (e) {
        e.preventDefault();
    });
    /*************************************** SCROLL CHANGE SECTION */
    if ((window.matchMedia("(min-width: 1300px)").matches) && (window.location.href.indexOf("about") != -1)) {
        $('body').bind('mousewheel', function (e) {
                if (e.originalEvent.wheelDelta / 120 > 0) {
                    console.log('scrolltop')
                    var num = $('.active').attr('data-section');
                    num--;
                    if (num <= 0) {
                        num = num + 5;
                    }
                    $('.active').removeClass('active');
                    $('.rempli').removeClass('rempli');
                    $('div[data-section="' + num + '"]').addClass('active');
                    $('div[data-slide="' + num + '"]').addClass('rempli');
                } else {
                    console.log('scrollbottom')
                    var num = $('.active').attr('data-section');
                    num++;
                    if (num >= 6) {
                        num = 1;
                    }
                    $('.active').removeClass('active');
                    $('.rempli').removeClass('rempli');
                    $('div[data-section="' + num + '"]').addClass('active');
                    $('div[data-slide="' + num + '"]').addClass('rempli');
                }
        });

        $(".carre").addClass("rempli");
        /* CLICK CARRE 1 CHANGE SECTION */
        $(".carre").click(function () {
            $("#about-team").addClass("active");
            $("div[data-section='2']").removeClass("active");
            $("div[data-section='3']").removeClass("active");
            $("div[data-section='4']").removeClass("active");
            $("div[data-section='5']").removeClass("active");
            $(".carre").addClass("rempli");
            $(".carre2").removeClass("rempli");
            $(".carre3").removeClass("rempli");
            $(".carre4").removeClass("rempli");
            $(".carre5").removeClass("rempli");
        });
        /* CLICK CARRE 2 CHANGE SECTION */
        $(".carre2").click(function () {
            $("#about-team").removeClass("active");
            $("div[data-section='2']").addClass("active");
            $("div[data-section='3']").removeClass("active");
            $("div[data-section='4']").removeClass("active");
            $("div[data-section='5']").removeClass("active");
            $(".carre").removeClass("rempli");
            $(".carre2").addClass("rempli");
            $(".carre3").removeClass("rempli");
            $(".carre4").removeClass("rempli");
            $(".carre5").removeClass("rempli");
        });
        /* CLICK CARRE 3 CHANGE SECTION */
        $(".carre3").click(function () {
            $("#about-team").removeClass("active");
            $("div[data-section='2']").removeClass("active");
            $("div[data-section='3']").addClass("active");
            $("div[data-section='4']").removeClass("active");
            $("div[data-section='5']").removeClass("active");
            $(".carre").removeClass("rempli");
            $(".carre2").removeClass("rempli");
            $(".carre3").addClass("rempli");
            $(".carre4").removeClass("rempli");
            $(".carre5").removeClass("rempli");
        });
        /* CLICK CARRE 4 CHANGE SECTION */
        $(".carre4").click(function () {
            $("#about-team").removeClass("active");
            $("div[data-section='2']").removeClass("active");
            $("div[data-section='3']").removeClass("active");
            $("div[data-section='4']").addClass("active");
            $("div[data-section='5']").removeClass("active");
            $(".carre").removeClass("rempli");
            $(".carre2").removeClass("rempli");
            $(".carre3").removeClass("rempli");
            $(".carre4").addClass("rempli");
            $(".carre5").removeClass("rempli");
        });
        /* CLICK CARRE 5 CHANGE SECTION */
        $(".carre5").click(function () {
            $("#about-team").removeClass("active");
            $("div[data-section='2']").removeClass("active");
            $("div[data-section='3']").removeClass("active");
            $("div[data-section='4']").removeClass("active");
            $("div[data-section='5']").addClass("active");
            $(".carre").removeClass("rempli");
            $(".carre2").removeClass("rempli");
            $(".carre3").removeClass("rempli");
            $(".carre4").removeClass("rempli");
            $(".carre5").addClass("rempli");
        });
    }
    if (window.matchMedia("(max-width: 1300px)").matches) {
        if (window.matchMedia("(max-width: 1300px)").matches) {
            $("#studio > div > div > div:nth-child(1)").addClass("active");
            $("#about-team").removeClass("img");
            /*************************************** SLIDER */
            const items = document.querySelectorAll('.img');
            const nbSlide = items.length;
            const suivant = $('.right-arrow');
            let count = 0;


            function slideSuivant() {
                items[count].classList.remove('active');

                if (count < nbSlide - 1) {
                    count++;
                } else {
                    count = 0;
                }

                items[count].classList.add('active');
            }
            suivant.click(slideSuivant);
        }
    }
    /*************************************** IF PAGE Mobile */
    if (window.location.href.indexOf("movies") != -1) {
        $("#page-name").text("FILMS");
        $("a#menu-film").css("opacity", "1");
    } else if (window.location.href.indexOf("about") != -1) {
        $("#page-name").text("ABOUT");
        $("a#menu-about").css("opacity", "1");
    } else {
        $("#page-name").text("WELCOME");
        $("a#menu-welcome").css("opacity", "1");
    }

    /*************************************** SCROLL CHANGE SECTION ACCUEIL */
    if (window.matchMedia("(min-width: 1330px)").matches) {
        if ($(".modal").css("display") == "none") {
        //     setInterval(function(){
        //             var num = $('.active').attr('data-section');
        //             num++;
        //             console.log(num);
        //             if (num >= 4) {
        //                 num = 1;
        //             }
        //             $('.active').removeClass('active');
        //             $('.rempli').removeClass('rempli');
        //             $('div[data-section="' + num + '"]').addClass('active');
        //             $('div[data-slide="' + num + '"]').addClass('rempli');
        //         }, 5000);
    }

    $(".slide1").addClass("rempli");
    /* CLICK CARRE 1 CHANGE SECTION */
    $(".slide1").click(function slide1() {
        $("#diapo1").addClass("active");
        $("#diapo2").removeClass("active");
        $("#diapo3").removeClass("active");
        $(".slide1").addClass("rempli");
        $(".slide2").removeClass("rempli");
        $(".slide3").removeClass("rempli");
    });
    /* CLICK CARRE 2 CHANGE SECTION */
    $(".slide2").click(function slide2() {
        $("#diapo1").removeClass("active");;
        $("#diapo2").addClass("active");
        $("#diapo3").removeClass("active");
        $(".slide1").removeClass("rempli");
        $(".slide2").addClass("rempli");
        $(".slide3").removeClass("rempli");
    });
    /* CLICK CARRE 3 CHANGE SECTION */
    $(".slide3").click(function slide3() {
        $("#diapo1").removeClass("active");
        $("#diapo2").removeClass("active");
        $("#diapo3").addClass("active");
        $(".slide1").removeClass("rempli");
        $(".slide2").removeClass("rempli");
        $(".slide3").addClass("rempli");
    });
}
/*************************************** SCROLL MENU FILM SECTION */
if ((window.matchMedia("(min-width: 1330px)").matches) && (window.location.href.indexOf("movies") != -1)) {
    window.addEventListener('scroll', () => {
        if (window.scrollY > 1) {
            $("header#masthead").addClass("scrolled");
            $("#categories").addClass("fixed");
        } else if (window.scrollY < 1) {
            $("header#masthead").removeClass("scrolled");
            $("#categories").removeClass("fixed");
        }
    });
}
});/*ready*/