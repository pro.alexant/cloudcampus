(function ($) {

    $(document).ready(function () {
        $('.cat-list_item').on('click', function () {
            $('.cat-list_item').removeClass('active');
            $(this).addClass('active');

            $.ajax({
                type: 'POST',
                url: wpAjax.ajaxUrl,
                dataType: 'html',
                data: {
                    action: 'filter_projects',
                    category: $(this).data('slug'),
                },
                success: function (res) {
                    $('.project-tiles').html(res);
                    console.log(res)
                }
            })
        });
    });
})(jQuery);