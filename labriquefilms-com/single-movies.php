<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package labriquefilms.com
 *
  Template Name: Film
 */

get_header();
?>
<style>
    header,
    footer {
        display: none;
    }
    @media screen and (max-width: 1050px){
        .container{
            width: 100%;
        }
        body{
            padding: 0;
        }
    }
</style>

<main id="primary" class="site-main">
    <section id="film">
        <div class="container">
            <div id="frame" class="mobile">
                <?= get_field('lien_vimeo') ?>
            </div>
            <div id="title-film">
                <h3><?php the_title() ?></h3>
                <h4><?php the_category(' '); ?></h4>
            </div>
            <div id="frame" class="fullscreen">
                <?= get_field('lien_vimeo') ?>
            </div>
            <div id="croix" class="fullscreen">
                <a href="<?php echo site_url(); ?>/movies/"><img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-plus.png" alt="Retour a la liste de Film"></a>
            </div>
            <div class="clear"></div>
            <div id="infos-prod">
                <p class='fullscreen'><?= get_field('realisateur') ?></p>
                <div class="mobile">
                <?= get_field('realisateur') ?>
                </div>
            </div>
            <div id="croix" class="mobile">
                <a href="<?php echo site_url(); ?>/movies/"><img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-plus.png" alt="Retour a la liste de Film"></a>
            </div>
    </section>
</main><!-- #main -->

<?php
get_footer();
