<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package design-finance
 *
  Template Name: Mention Legal
 */

  get_header();
  ?>

  <main id="primary" class="site-main">
    <section id="legal">
        <div class="container">
            <div id="legals" class="text-center">
                <h2>LA BRIQUE FILMS</h2>
                <p>Société : 89363793400017</p>
                <p>109 RUE DUHESME 75018 PARIS</p>
                <p>75018 PARIS </p>
                <div>
                    <div>Mentions Légales & Politique de confidentialité</div>
                </div>
                <div id="legal-content">
                    <p>Conformément aux dispositions de l’article 6 de la loi n° 2004-575 du 21 juin 2004 pour la confiance dans l’économie numérique, nous vous informons que :</p>
                    <p>Le site internet <b>www.labriquefilms.com</b> est la propriété de:</p>
                    <div><b>Société </b>: LA BRIQUE FILM</div>
                    <div><b>Adresse </b>: LA BRIQUE FILM, 41 rue Joseph de Maistre 75018 Paris</div>
                    <div><b>SIREN </b>: 893637934</div>
                    <div><b>SIRET (siege) </b>: 89363793400025</div>
                    <div><b>Forme Juridique </b>: Société par actions simplifiée</div>
                    <p>Le directeur de la publication est : <br><b>Benoit BUREAU</b> est président et <b>Nils JACOBSEN</b> est directeur général de l'entreprise LA BRIQUE FILMS.</p>
                    <p>Le site est hébergé par OVH, 2 rue Kellermann – 59100 Roubaix – France</p> <br>
                    <hr><br>
                    <h2>Conception & Développement</h2>
                    <p> <b>ABC CONCEPTION</b> <br> <a href="https://www.abc-conception.com/" target="_blank" rel="noopener">Création de sites - Référencement</a></p>
                    <p> <a href="https://www.abc-conception.com/" target='_blank' rel='noopenner'><b>Développement Web</b> : Thomas Alexandre</p><br></a>
                    <hr><br>
                    <h3>Politique de Confidentialité</h3>
                    <h4>Données personnelles</h4>
                    <p>Dans une logique de respect de la vie privée de ses Utilisateurs, <b>https://labriquefilms.com</b> s’engage à ce que la collecte et le traitement d’informations personnelles, effectués au sein du présent site, soient effectués conformément à la loi n°78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés, dite Loi « Informatique et Libertés ».</p>
                    <p>A ce titre, le site <b>https://labriquefilms.com</b> fait l’objet d’une dispense de déclaration à la CNIL (Dispense n°7 : Délibération n°2006-138 du 9 mai 2006 décidant de la dispense de déclaration des traitements constitués à des fins d’information ou de communication externe).</p>
                    <p>Conformément à l’article 34 de la loi « Informatique et Libertés », <b>https://labriquefilms.com</b> garantit à l’utilisateur un droit d’opposition, d’accès et de rectification sur les données nominatives le concernant.</p>
                    <p>L’utilisateur a la possibilité d’exercer ce droit en utilisant le formulaire de contact mis à sa disposition depuis la page contact du site.</p>
                    <h4>Balises de tracking Analytics</h4>
                    <p>Nous utilisons un système de récolte de données de navigations (Google Analytics) avec pour unique but de connaitre le taux de fréquentation et statistiques globales du site <b>https://labriquefilms.com</b>.</p>
                    <p>Ces données sont totalement anonymes et sont conservées un maximum de 13 mois.</p>
                    <h4>Utilisation des données personnelles collectées</h4><b>Informations de contact</b>
                    <p>Les informations récoltées via les formulaires de contact du site ou de quelque manière sont uniquement destinées à vous recontacter et demeurent confidentielles.</p>
                    <p>Sur simple demande nous pouvons vous supprimer de notre fichier de contact.</p>
                    <h4>Gestion des cookies</h4>
                    <p>Lors de la navigation sur le site <b>https://labriquefilms.com</b>, des cookies sont implantés dans votre navigateur. Ces cookies sont des données de navigations uniquement stockées sur votre navigateur. Nous ne sauvegardons, ni utilisons ces données.</p>
                    <p>Nous vous informons que vous pouvez vous opposer à l’enregistrement de cookies en configurant votre navigateur de la manière suivante :</p>
                    <p>1. Choisissez le menu « Outils » (ou « Tools ), puis « Options Internet » (ou « Internet Options »)</p>
                    <p>2. Cliquez sur l’onglet » Confidentialité » (ou » Confidentiality « )</p>
                    <p>3. Sélectionnez le niveau souhaité à l’aide du curseur</p>
                    <h4>Pour Mozilla Firefox :</h4>
                    <p>1. Choisissez le menu « Outils » > « Options »</p>
                    <p>2. Cliquez sur l’option » Vie Privée »</p>
                    <p>3. Rubrique » Cookies «</p>
                    <h4>Pour Chrome :</h4>
                    <p>1. Choisissez le menu Edition » > « Préférences »</p>
                    <p>2. Cliquez sur l’option « Données personnelles »</p>
                    <p>3. Rubrique « Cookies »</p>
                    <h4>Pour Safari :</h4>
                    <p>1. Choisissez le menu « Edition » > « Préférences »</p>
                    <p>2. Cliquez sur l’option « Données personnelles »</p>
                    <p>3. Rubrique « Cookies »</p>
                    <h4>Pour iOS :</h4>
                    <p>1. Cliquez sur l’icône « Réglages »</p>
                    <p>2. Cliquez sur l’option « Safari »3. Rubrique « CONFIDENTIALITÉ ET SÉCURITÉ »</p>
                    <h4>Pour Android :</h4>
                    <p>1. Choisissez le menu « Edition » > « Paramètres »</p>
                    <p>2. Cliquez sur l’option « Confidentialité et sécurité »</p>
                    <p>3. Rubrique « Cookies »</p>
                    <h4>Pour Windows phone :</h4>
                    <p>1. Choisissez le menu « Edition » > « Préférences »</p>
                    <p>2. Cliquez sur l’option « paramètres avancés »</p>
                    <p>3. Rubrique « Cookies de sites Web et d’applications »</p>
                    <h4>Pour Chrome mobile :</h4>
                    <p>1. Choisissez le menu « Edition » > « Paramètres »</p>
                    <p>2. Cliquez sur l’option « Confidentialité »</p>
                    <p>3. Rubrique « Effacer données navigation »</p>
                    <br>
                </div>
                <!--.#legals-contents-->
            </div>
            <!--.#legals-->
        </div>
    </section>
</main>


<?php
get_footer();
