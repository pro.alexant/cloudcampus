<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package labriquefilms.com
 */

?>

<footer id="colophon" class="site-footer">
    <div id="footer-content" class="container text-center">
      <div id="infos" class='fullscreen'>
        <a href="mailto:hello@labriquefilms.com ">hello@labriquefilms.com</a> | <a href="tel:09 84 35 34 13">09 84 35 34 13</a> | <a href="https://goo.gl/maps/6ZjaADQVkohvtqTC7" target="blank">221 Rue Championnet 75018 PARIS</a>
        <div id="copyright">© Copyright 2021 La Brique Films | <a id="legal-link" href="<?php echo site_url(); ?>/mentions-legales">Mentions légales</div></a> 
      </div>
    </div><!--#footer-content-->
</footer><!-- #colophon -->
<?php wp_footer(); ?>

</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="<?= get_stylesheet_directory_uri(); ?>/js/app.js"></script>
<script type="text/javascript">
  /*********************************** MENU MOBILE */
  $('#burger').click(function(e) {
    $('header').toggleClass('open');
    $('.icon').toggleClass('open');
  });
  $('.navbar-mobile  a').click(function(e) {
    $('header').removeClass('open');
    $('.icon').removeClass('open');
  }); /* menu mobile */
  /*********************************** FILTRE MOBILE */
  $('.plus.icon').click(function(e) {
    $('ul.cat-list').toggleClass('actif');
    $('.plus.icon').toggleClass('remove');
    $('header').toggleClass('hidden');
    $('.categorymobile span').toggleClass('hidden');
    $('#all a').toggleClass('hidden');
  });
  $('a.cat-list_item').click(function(e) {
    $('ul.cat-list').toggleClass('actif');
    $('.plus.icon').toggleClass('remove');
    $('header').removeClass('hidden');
    $('.categorymobile span').toggleClass('hidden');
    $('#all a').toggleClass('hidden');
  });
  $('#all a').click(function(e) {
    $('ul.cat-list').removeClass('actif');
    $('.plus.icon').removeClass('remove');
    $('header').removeClass('hidden');
    $('.categorymobile span').toggleClass('hidden');
    $('#all a').toggleClass('hidden');
  });
  /* filtre mobile */
  $('#ben-plus').click(function() {
    $('#infos-nils').toggleClass("active");
  });
  $('#nils-plus').click(function() {
    $('#infos-ben').toggleClass("active");
  });
</script>

</html>