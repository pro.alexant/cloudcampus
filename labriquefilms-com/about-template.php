<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package design-finance
 *
  Template Name: About
 */

get_header();
?>

<style>
    footer#colophon {
        width: 100%;
        text-align: center;
        top: inherit;
        bottom: 0;
        position: fixed;
    }
</style>

<?php $page = 50; ?>
<main id="primary" class="site-main">
    <div data-section="1" id="about-team" class="img active">
        <section id="landing">
            <div class="container">
                <div id="about" class="col-20">
                    <div id="lateam" class='fullscreen'>LA <br>TEAM</div>
                    <div id="lateam" class='mobile'>LA TEAM</div>
                    <div id="laben">CALL ME BEN</div>
                    <div id="lanils">HI I'M <br>NILS</div>
                </div>
                <div class="col-70">
                    <div class="col-2">
                        <div class="wrapper">
                            <img class="about-photo" src="<?= the_field('photo_benoit', $page); ?>" alt="Benoit Bureau<?= the_field('poste_benoit', $page); ?>">
                            <div class="ben-overlay">
                                <div class="content">
                                    <p class="nom">Benoit Bureau</p>
                                    <p class="job"><?= the_field('poste_benoit', $page); ?></p>
                                    <a href="tel:+33698264868" class="num">
                                        <p><?= the_field('telephone_benoit', $page); ?></p>
                                    </a>
                                    <a href="mailto:nils@labriquefilms.com" class="email">
                                        <p><?= the_field('email_benoit', $page); ?></p>
                                    </a>
                                </div>
                            </div>
                            <div id="ben-plus">
                                <img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-plus.png" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="wrapper">
                            <img class="about-photo" src="<?= the_field('photo_nils', $page); ?>" alt="Nils Jacobsen <?= the_field('poste_nils', $page); ?>">
                            <div class="nils-overlay">
                                <div class="content">
                                    <p class="nom">Nils Jacobsen</p>
                                    <p class="job"><?= the_field('poste_nils', $page); ?></p>
                                    <a href="tel:+33698264868" class="num">
                                        <p><?= the_field('telephone_nils', $page); ?></p>
                                    </a>
                                    <a href="mailto:nils@labriquefilms.com" class="email">
                                        <p><?= the_field('email_nils', $page); ?></p>
                                    </a>
                                </div>
                            </div>
                            <div id="nils-plus">
                                <img src="<?= get_stylesheet_directory_uri(); ?>/img/icon-plus.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <section id="studio">
        <div class="container">
            <div class="slider">
                <div data-section="2" class="img">
                    <!-- SLIDER STUDIO IMG 1 -->
                    <div class="col-20">
                        <div id="title-studio">
                            <div class='le-studio'>Le studio</div class='le-studio'>
                            <div id="img-studio" class='mobile'>
                                <img src="<?= the_field('photo_slider_1', $page); ?>" alt="Le Studio">
                            </div>
                            <p class="fullscreen"><?= the_field('description_slider_1', $page); ?></p>
                            <p class="mobile"><?= the_field('description_slider_1', $page); ?></p>
                            <div class="slideposition">
                                <div class="colonne-2">
                                    <div>1/4</div>
                                </div>
                                <div class="colonne-2">
                                    <div class="right-arrow"><img src="<?= get_stylesheet_directory_uri(); ?>/img/right-arrow.png" alt=""></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-70">
                        <div id="img-studio" class='fullscreen'>
                            <img src="<?= the_field('photo_slider_1', $page); ?>" alt="Le Studio">
                        </div>
                    </div>
                </div>
                <!-- SLIDER STUDIO IMG 2 -->
                <div data-section="3" class="img">
                    <div class="col-20">
                        <div id="title-studio">
                            <div class='le-studio'>Le studio</div class='le-studio'>
                            <div id="img-studio" class='mobile'>
                                <img src="<?= the_field('photo_slider_2', $page); ?>" alt="Le Studio">
                            </div>
                            <p class="fullscreen"><?= the_field('description_slider_2', $page); ?></p>
                            <p class="mobile"><?= the_field('description_slider_2', $page); ?></p>
                            <div class="slideposition">
                                <div class="colonne-2">
                                    <div>2/4</div>
                                </div>
                                <div class="colonne-2">
                                    <div class="right-arrow"><img src="<?= get_stylesheet_directory_uri(); ?>/img/right-arrow.png" alt=""></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-70">
                        <div id="img-studio" class='fullscreen'>
                            <img src="<?= the_field('photo_slider_2', $page); ?>" alt="Le Studio">
                        </div>
                    </div>
                </div>
                <!-- SLIDER STUDIO IMG 3 -->
                <div data-section="4" class="img">
                    <div class="col-20">
                        <div id="title-studio">
                            <div class='le-studio'>Le studio</div class='le-studio'>
                            <div id="img-studio" class='mobile'>
                                <img src="<?= the_field('photo_slider_3', $page); ?>" alt="Le Studio">
                            </div>
                            <p class="fullscreen"><?= the_field('description_slider_3', $page); ?></p>
                            <p class="mobile"><?= the_field('description_slider_3', $page); ?></p>
                            <div class="slideposition">
                                <div class="colonne-2">
                                    <div>3/4</div>
                                </div>
                                <div class="colonne-2">
                                    <div class="right-arrow"><img src="<?= get_stylesheet_directory_uri(); ?>/img/right-arrow.png" alt=""></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-70">
                        <div id="img-studio" class='fullscreen'>
                            <img src="<?= the_field('photo_slider_3', $page); ?>" alt="Le Studio">
                        </div>
                    </div>
                </div>
                <!-- SLIDER STUDIO IMG 4 -->
                <div data-section="5" class="img">
                    <div class="col-20">
                        <div id="title-studio">
                            <div class='le-studio'>Le studio</div class='le-studio'>
                            <div id="img-studio" class='mobile'>
                                <img src="<?= the_field('photo_slider_4', $page); ?>" alt="Le Studio">
                            </div>
                            <p class="fullscreen"><?= the_field('description_slider_4', $page); ?></p>
                            <p class="mobile"><?= the_field('description_slider_4', $page); ?></p>
                            <div class="slideposition">
                                <div class="colonne-2">
                                    <div>4/4</div>
                                </div>
                                <div class="colonne-2">
                                    <div class="right-arrow"><img src="<?= get_stylesheet_directory_uri(); ?>/img/right-arrow.png" alt=""></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-70">
                        <div id="img-studio" class='fullscreen'>
                            <img src="<?= get_stylesheet_directory_uri(); ?>/img/camera.jpg" alt="Le Studio">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div id="scroll-about">
        <div data-slide="1" class="carre"></div>
        <div data-slide="2" class="carre2"></div>
        <div data-slide="3" class="carre3"></div>
        <div data-slide="4" class="carre4"></div>
        <div data-slide="5" class="carre5"></div>
    </div>
</main><!-- #main -->

<?php
get_footer();
?>

<script>
    $(document).ready(function() {
        /************************************  BEN BUTTON CLICK */
        $("#ben-plus").click(function() {
            $("#ben-plus > img").toggleClass("click");
            $(".ben-overlay").toggleClass("visible");
            $("#laben").toggleClass("hidden");
            $("#lanils").toggleClass("hidden");
            $("#lateam").toggleClass("hidden");
        });
        /************************************  NILS BUTTON CLICK */
        $("#nils-plus").click(function() {
            $("#nils-plus > img").toggleClass("click");
            $(".nils-overlay").toggleClass("visible");
            $("#laben").toggleClass("hidden");
            $("#lanils").toggleClass("hidden");
            $("#lateam").toggleClass("hidden");
        });
    }); /*ready*/
</script>