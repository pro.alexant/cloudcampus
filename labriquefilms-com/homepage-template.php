<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package design-finance
 *
  Template Name: Accueil
 */

  get_header();
  ?>

  <?php
  function wpb_latest_sticky()
  {

    /* Get all sticky posts */
    $sticky = get_option('sticky_posts');

    /* Sort the stickies with the newest ones at the top */
    rsort($sticky);

    /* Get the 5 newest stickies (change 5 for a different number) */
    $sticky = array_slice($sticky, 0, 3);

    /* Query sticky posts */
    $the_query = new WP_Query(array('post__in' => $sticky, 'ignore_sticky_posts' => 1, 'post_type' => 'movies'));
    // The Loop
    if ($the_query->have_posts()) {
        $i = 0;
        echo '<div class="slider">';
        while ($i++ < 3 && $the_query->have_posts()) {
            $the_query->the_post();
            include('template-parts/slider.php');
            
        }
        echo '</div>';
    } else {
        echo "<div>Aucun post trouvé</div>";
    }
}
?>

<div id="loading">
    <div id="logo-load">
        <img src="<?= get_stylesheet_directory_uri(); ?>/img/openning-labrique-v2-02-2022.gif" alt="La Brique Films">
    </div>
</div>
<main id="primary" class="site-main">
    <section id="home">
        <div class="container">
            <?php wpb_latest_sticky(); ?>
            <div id="scroll">
                <div data-slide="1" class="slide1"></div>
                <div data-slide="2" class="slide2"></div>
                <div data-slide="3" class="slide3"></div>
            </div>
        </div>
    </section>
    <!--#landing-->
</main><!-- #main -->
<script>
    jQuery(document).ready(function($) {
        if (sessionStorage.getItem('session')) {
            $("#loading").css("display", "none");
            $("body").css("padding", "10px 20px");
        }
        sessionStorage.setItem( 'session', 'true' );

        $("body").css("padding", "0");

        function loading() {
            $("#loading").css("display", "none");
            $("body").css("padding", "10px 20px");
        }

        setTimeout(loading, 2300);

    });
</script>

<?php
get_footer();
