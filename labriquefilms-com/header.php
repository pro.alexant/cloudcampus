<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package labriquefilms.com
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Création de contenu audiovisuels publicitaires, court-métrages, clips vidéo.  Tel:  09 84 35 34 13 | 221 Rue Championnet 75018 PARIS">
	<!--opengraph--->
	<meta property="og:locale" content="fr_FR">
	<meta property="og:type" content="website">
	<meta property="og:title" content="La Brique Films // Studio de Création audiovisuelle">
	<meta property="og:description" content="Création de contenu audiovisuels publicitaires, court-métrages, clips vidéo.  Tel:  09 84 35 34 13 | 221 Rue Championnet 75018 PARIS">
	<meta property="og:url" content="https://labriquefilms.com/">
	<meta property="og:image" content="<?= get_stylesheet_directory_uri(); ?>/img/opengraph.jpg">
	<meta property="og:site_name" content="La Brique Films">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="apple-touch-icon" sizes="180x180" href="<?= get_stylesheet_directory_uri(); ?>/img/favicon/.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?= get_stylesheet_directory_uri(); ?>/img/favicon/favicon.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?= get_stylesheet_directory_uri(); ?>/img/favicon/.png">
	<link rel="shortcut icon" type="image/png" href="<?= get_stylesheet_directory_uri(); ?>/img/favicon/favicon.png">
	<link rel="manifest" href="<?= get_stylesheet_directory_uri(); ?>/img/favicon/site.webmanifest">

	<?php wp_head(); ?>
</head>
<?php $page = 124; //chargement du logo 
?>

<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>
	<div id="page" class="site">
		<header id="masthead" class="site-header">
			<div class="container">
				<div id="logo" class="col-3 text-left">
					<a class='fullscreen' href="<?php echo site_url(); ?>/index.php"><img src="<?php the_field('logo', $page); ?>" alt="Logo La Brique Film"></a>
					<a id="page-name" class='mobile'></a>
				</div>
				<!--#logo-->
				<div class="fullscreen">
					<nav id="menu-link" class="col-3 text-center">
						<a id="menu-welcome" href="<?php echo site_url(); ?>/index.php">WELCOME</a>
						<a id="menu-film" href="<?php echo site_url(); ?>/movies/">FILMS</a>
						<a id="menu-about" href="<?php echo site_url(); ?>/about/">ABOUT</a>
					</nav>
					<!--#menu-link-->

					<div id="reseaux" class="col-3">
						<a id="vimeo" href="<?php the_field('vimeo', $page); ?>" target='blank'><img src="<?= get_stylesheet_directory_uri(); ?>/img/vimeo.jpg" alt="Page Vimeo La Brique Film"></a>
						<a id="instagram" href="<?php the_field('instagram', $page); ?>" target='blank'><img src="<?= get_stylesheet_directory_uri(); ?>/img/insta.jpg" alt="Page Instagram La Brique Film"></a>
						<a id="facebook" href="<?php the_field('facebook', $page); ?>" target='blank'><img src="<?= get_stylesheet_directory_uri(); ?>/img/fb.jpg" alt="Page Facebook La Brique Film"></a>
					</div>
					<!--#reseaux-->
				</div>
				<!--.fullscreen-->
				<!--MOBILE-->
				<div id="burger">
					<div class="menu icon"></div>
				</div>
				<!--#burger-->
			</div>
			<!--.container-->
			<div class="navbar-mobile">
				<div class="container">
					<nav id="menu-link" class="col-3">
						<a id="menu-welcome" href="<?php echo site_url(); ?>/index.php">WELCOME</a>
						<a id="menu-film" href="<?php echo site_url(); ?>/movies/">FILMS</a>
						<a id="menu-about" href="<?php echo site_url(); ?>/about/">ABOUT</a>
					</nav>
					<!--#menu-link-->
					<div id="reseaux" class="col-3 text-right">
						<a id="vimeo" href="<?php the_field('vimeo', $page); ?>" target='blank'><img src="<?= get_stylesheet_directory_uri(); ?>/img/vimeo.jpg" alt="Page Vimeo La Brique Film"></a>
						<a id="instagram" href="<?php the_field('instagram', $page); ?>" target='blank'><img src="<?= get_stylesheet_directory_uri(); ?>/img/insta.jpg" alt="Page Instagram La Brique Film"></a>
						<a id="facebook" href="<?php the_field('facebook', $page); ?>" target='blank'><img src="<?= get_stylesheet_directory_uri(); ?>/img/fb.jpg" alt="Page Facebook La Brique Film"></a>
					</div>
					<!--#reseaux-->
					<div id="infos">
						<a href="mailto:<?php the_field('contact_mail', $page); ?> "><?php the_field('contact_mail', $page); ?></a>
						<br>-<br>
						<a href="tel:<?php the_field('telephone', $page); ?>"><?php the_field('telephone', $page); ?></a>
						<br>-<br>
						<a href="https://goo.gl/maps/6ZjaADQVkohvtqTC7" target="blank"><?php the_field('adresse', $page); ?></a>
						<br>
						<div id="copyright">Mentions légales <br>-<br> © Copyright 2021 La Brique Films</div>
					</div>
					<!--#infos-->
				</div>
				<!--.container-->
			</div>
			<!--#navbar-mobile-->
			<!--MOBILE-->

		</header><!-- #masthead -->